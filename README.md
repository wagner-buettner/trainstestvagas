# The CrazyTrains

## Introduction
The objective of this project is calculate paths between two cities, represented by one letter per city, and try to answer some questions:

The purpose of this project, is to help the railroad provide its customers with information about
the routes. In particular, compute the distance along a certain route, the number of 
different routes between two towns, and the shortest route between two towns.

You will provide two Files to get the ticket for the CrazyTrain:
- A file, named ```routes.txt```, with routes and distances
- A file, named ```questions.txt```, with questions about your trips

And father Ozzy will answer your questions!

## System requirements

- I made this project with Java 8, but Java 7 should be fine too
- Same for Operational System. I used Ubuntu 17.04, but Mac and Windows must be ok
- Apache Maven 3.X (I used 3.5)

## Building project

This is a Apache Maven project.
So, get into the project folder and ```mvn clean install```

After this, copy ```routes.txt and questios.txt``` to target folder

## Code Coverage

To get the Code Coverage Report, just run: ```mvn verify```. A report will be created at target folder:

    target/site/jacoco/index.html
    
## Running Checkstyles

To see if your forked repository has any ```PMD or Findbugs```, run:

    mvn pmd:check
    mvn findbugs:check

## About Input Files

####Routes.txt 
must have one route per line

```
XY10
WA5
AB3
```
Where XY10 it's a graph from X to Y with 10 distance.


####Questions.txt
must have one question per line, like the examples below:

- The distance of the route W-A-G
- The number of trips starting at A and ending at B with a maximum of 10 stops
- The number of trips starting at W and ending at A with exactly 4 stops
- The length of the shortest route from A to B
- The length of the shortest route from D to E
- The number of different routes from A to C with a distance of less than 45

###Questions outputs will be shown on the screen.

## Executing project

- Get in ```target``` folder inside the project (after building it with maven)
- Copy ```routes.txt``` and ```question.txt``` files given in the project folder root into: ```target``` folder
- Edit routes and questions if you want
- Run jar file: ```java -jar crazytrains-0.1.jar```

##Logs 
- A Log Folder is created by log4j2 while the application is executed