package br.com.vagas.trains.domain;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author wagner on 22/08/17
 */
public class RouteTest {
    private Route route;
    private final String origin = "X";
    private final String destination = "Y";
    private final Integer distance = 10;


    @Before
    public void setUp() throws Exception {
        route = new Route(origin, destination, distance);
    }

    @Test
    public void testIfGetIdIsXY() throws Exception {
        String id = "XY";
        Assert.assertEquals(id, route.getId());
    }

    @Test
    public void testIfGetOriginIsX() throws Exception {
        Assert.assertEquals(origin, route.getOrigin());
    }

    @Test
    public void testIfGetDestinationIsY() throws Exception {
        Assert.assertEquals(destination, route.getDestination());
    }

    @Test
    public void testIfGetDistanceIs10() throws Exception {
        Assert.assertEquals(distance, route.getDistance());
    }
}
