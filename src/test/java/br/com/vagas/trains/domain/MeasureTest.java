package br.com.vagas.trains.domain;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author wagner on 21/08/17
 */
public class MeasureTest {

    private final String origin = "W";
    private final String destination = "W";
    private final String graph = "W-A-G";
    private final Integer value = 1910;
    private final String type = "MAX-STOPS";

    private Measure measure;

    @Before
    public void setUp() throws Exception {
        measure = new Measure();
    }

    @Test
    public void testIfGetOriginIsW() {
        measure.setOrigin(origin);
        Assert.assertEquals(origin, measure.getOrigin());
    }

    @Test
    public void testSetOriginWithW() {
        measure.setOrigin(origin);
        Assert.assertEquals(origin, measure.getOrigin());
    }

    @Test
    public void testIfGetDestinationIsY() throws Exception {
        measure.setDestination(destination);
        Assert.assertEquals(destination, measure.getDestination());
    }

    @Test
    public void testSetDestinationWithY() throws Exception {
        measure.setDestination(destination);
        Assert.assertEquals(destination, measure.getDestination());
    }

    @Test
    public void testIfGetValueIs1910() throws Exception {
        measure.setValue(value);
        Assert.assertEquals(value, measure.getValue());
    }

    @Test
    public void testSetValueWith1910() throws Exception {
        measure.setValue(value);
        Assert.assertEquals(value, measure.getValue());
    }

    @Test
    public void testIfGetGraphIsWAG() throws Exception {
        measure.setGraph(graph);
        Assert.assertEquals(graph, measure.getGraph());
    }

    @Test
    public void testSetGraphWithWAG() throws Exception {
        measure.setGraph(graph);
        Assert.assertEquals(graph, measure.getGraph());
    }

    @Test
    public void testIfGetTypeIsMAXSTOPS() throws Exception {
        measure.setType(type);
        Assert.assertEquals(type, measure.getType());

    }

    @Test
    public void testSetTypeWithMAXSTOPS() throws Exception {
        measure.setType(type);
        Assert.assertEquals(type, measure.getType());

    }
}
