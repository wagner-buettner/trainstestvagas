package br.com.vagas.trains.domain;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author wagner on 22/08/17
 */
public class TripTest {

    private Trip trip;
    private final String graph = "XY";
    private final Integer stops = 3;
    private final Integer distance = 10;

    @Before
    public void setUp() throws Exception {
        trip = new Trip(graph, stops, distance);
    }

    @Test
    public void testIfTripConstructorWithGraphXYWith3StopsWith10Distance() {
        trip = new Trip(graph, stops, distance);
        Assert.assertEquals(graph, trip.getGraph());
        Assert.assertEquals(stops, trip.getStops());
        Assert.assertEquals(distance, trip.getDistance());
    }

    @Test
    public void testIfGetStopsWith3() throws Exception {
        trip = new Trip(graph, stops, distance);
        Assert.assertEquals(stops, trip.getStops());
    }

    @Test
    public void testIfGetDistanceWith10() throws Exception {
        trip = new Trip(graph, stops, distance);
        Assert.assertEquals(distance, trip.getDistance());
    }

}
