package br.com.vagas.trains.domain;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author wagner on 21/08/17
 */
public class PathTest {

    private Path path = new Path();

    @Before
    public void setUp() throws Exception {
        path = new Path();
    }

    @Test
    public void testIfAddRouteXY30() throws Exception {
        path.addRoute(new Route("X","Y", 30));
        Assert.assertTrue(path.getRoutes().containsKey("XY"));
    }

    @Test
    public void testIfCalculatePathDistanceIs135AndStopsIs4() throws Exception {
        path.addRoute(new Route("X","Y",50));
        path.addRoute(new Route("W","A",30));
        path.addRoute(new Route("G","N",40));
        path.addRoute(new Route("E","R",15));
        path.calculatePathDistanceAndStops();
        Assert.assertEquals(Integer.valueOf(135), path.getDistance());
        Assert.assertEquals(Integer.valueOf(4), path.getStops());
    }

    @Test
    public void testIfRemoveRouteTrue() throws Exception {
        Route route = new Route("X", "Y", 50);
        path.addRoute(route);
        path.removeRoute(route);
        Assert.assertFalse(path.getRoutes().containsKey("XY"));
    }

    @Test
    public void testIfGetPathWAGNERIsTrue() throws Exception {
        path.addRoute(new Route("W","A",10));
        path.addRoute(new Route("G","N",15));
        path.addRoute(new Route("E","R",3));
        Assert.assertEquals("WAGNER", path.getPath());
    }

    @Test
    public void testIfGetRoutesNotExists() {
        path.addRoute(new Route("P", "A", 100));
        path.addRoute(new Route("L", "M", 100));
        path.addRoute(new Route("E", "I", 100));
        path.addRoute(new Route("R", "A", 100));
        path.addRoute(new Route("S", "S", 100));
        Assert.assertFalse(path.getRoutes().containsKey("MUNDIAL"));
    }

    @Test
    public void testIfGetDistanceIs70() throws Exception {
        path.addRoute(new Route("W", "A", 50));
        path.addRoute(new Route("G", "N", 20));
        Assert.assertEquals(Integer.valueOf(70), path.getDistance());
    }

    @Test
    public void testIfGetStopsHas3Stops() throws Exception {
        path.addRoute(new Route("W","A",10));
        path.addRoute(new Route("G","N",15));
        path.addRoute(new Route("E","R",3));
        Assert.assertEquals(Integer.valueOf(3), path.getStops());
    }

}
