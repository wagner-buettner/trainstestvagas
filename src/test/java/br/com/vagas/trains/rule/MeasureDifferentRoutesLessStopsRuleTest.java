package br.com.vagas.trains.rule;

import br.com.vagas.trains.domain.Measure;
import br.com.vagas.trains.service.TripService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author wagner on 23/08/17
 */

public class MeasureDifferentRoutesLessStopsRuleTest {
    private MeasureDifferentRoutesLessStopsRule measureDifferentRoutesLessStopsRule;
    private Measure measure;
    private TripService tripService;

    private static final String type = "DIFFERENT-ROUTES-WITH-LESS-STOPS";
    private static final String question = "The number of different routes from A to B with a distance of less than 14";

    @Before
    public void setUp() throws Exception {
        measureDifferentRoutesLessStopsRule = new MeasureDifferentRoutesLessStopsRule();
        measure = new Measure();
        tripService = new TripService();

        tripService.loadRouteFile("routes.txt");
        measure.setType(type);
    }

    @Test
    public void testIfApplyWithAB14() throws Exception {
        measure = measureDifferentRoutesLessStopsRule.apply(question);
        Assert.assertEquals("AB14", measure.getOrigin()+measure.getDestination()+measure.getValue());
    }

    @Test
    public void getDifferentRoutesWithLessStops() throws Exception {
        measure = measureDifferentRoutesLessStopsRule.apply(question);
        Assert.assertEquals(Integer.valueOf(3), measureDifferentRoutesLessStopsRule.getDifferentRoutesWithLessStops(tripService));
    }
}
