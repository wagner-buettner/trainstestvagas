package br.com.vagas.trains.rule;

import br.com.vagas.trains.domain.Measure;
import br.com.vagas.trains.service.TripService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author wagner on 23/08/17
 */
public class MeasureTripsExactlyStopsRuleTest {
    private MeasureTripsExactlyStopsRule exactlyStopsRule;
    private Measure measure;
    private TripService tripService;

    private static final String type = "EXACTLY-STOPS";
    private static final String question = "The number of trips starting at A and ending at E with exactly 3 stops";

    @Before
    public void setUp() throws Exception {
        exactlyStopsRule = new MeasureTripsExactlyStopsRule();
        measure = new Measure();
        tripService = new TripService();

        tripService.loadRouteFile("routes.txt");
        measure.setType(type);
    }

    @Test
    public void testIfApplyWithAE3() throws Exception {
        measure = exactlyStopsRule.apply(question);
        Assert.assertEquals("AE3", measure.getOrigin()+measure.getDestination()+measure.getValue());
    }

    @Test
    public void testIfGetExactlyStopsWithAE3() throws Exception {
        measure = exactlyStopsRule.apply(question);
        Assert.assertEquals(Integer.valueOf(2), exactlyStopsRule.getExactlyStops(tripService));
    }

}
