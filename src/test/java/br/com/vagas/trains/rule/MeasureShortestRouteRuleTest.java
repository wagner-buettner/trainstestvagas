package br.com.vagas.trains.rule;

import br.com.vagas.trains.domain.Measure;
import br.com.vagas.trains.service.TripService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author wagner on 23/08/17
 */
public class MeasureShortestRouteRuleTest {
    private MeasureShortestRouteRule measureShortestRouteRule;
    private Measure measure;
    private TripService tripService;

    private static final String type = "SHORTEST-ROUTE";
    private static final String question = "The length of the shortest route from A to E";

    @Before
    public void setUp() throws Exception {
        measureShortestRouteRule = new MeasureShortestRouteRule();
        measure = new Measure();
        tripService = new TripService();

        tripService.loadRouteFile("routes.txt");
        measure.setType(type);
    }

    @Test
    public void testIfApplyWithAE() throws Exception {
        measure = measureShortestRouteRule.apply(question);
        Assert.assertEquals("AE", measure.getOrigin()+measure.getDestination());
    }

    @Test
    public void testIfGetShortestRouteWithAE3() throws Exception {
        measure = measureShortestRouteRule.apply(question);
        Assert.assertEquals(Integer.valueOf(7), measureShortestRouteRule.getShortestRoute(tripService));
    }

}
