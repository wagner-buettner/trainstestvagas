package br.com.vagas.trains.rule;

import br.com.vagas.trains.domain.Measure;
import br.com.vagas.trains.service.TripService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author wagner on 23/08/17
 */
public class MeasureDistanceRuleTest {
    private MeasureDistanceRule measureDistanceRule;
    private Measure measure;
    private TripService tripService;

    private static final String type = "DISTANCE";
    private static final String question = "The distance of the route A-E";

    @Before
    public void setUp() throws Exception {
        measureDistanceRule = new MeasureDistanceRule();
        measure = new Measure();
        tripService = new TripService();

        tripService.loadRouteFile("routes.txt");
        measure.setType(type);
    }

    @Test
    public void testIfApplyWithAEIsTrue() throws Exception {
        measure = measureDistanceRule.apply(question);
        Assert.assertEquals("A-E", measure.getGraph());
    }

    @Test
    public void testIfConvertStringGraphToDistanceWithAE7() throws Exception {
        measure = measureDistanceRule.apply(question);
        Assert.assertEquals(Integer.valueOf(7), measureDistanceRule.convertStringGraphToDistance(measure.getGraph(), tripService));
    }

}
