package br.com.vagas.trains.rule;

import br.com.vagas.trains.domain.Measure;
import br.com.vagas.trains.service.TripService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author wagner on 23/08/17
 */
public class MeasureMaxStopsRuleTest {
    private MeasureMaxStopsRule measureMaxStopsRule;
    private Measure measure;
    private TripService tripService;

    private static final String type = "MAX-STOPS";
    private static final String question = "The number of trips starting at A and ending at B with a maximum of 10 stops";

    @Before
    public void setUp() throws Exception {
        measureMaxStopsRule = new MeasureMaxStopsRule();
        measure = new Measure();
        tripService = new TripService();

        tripService.loadRouteFile("routes.txt");
        measure.setType(type);
    }

    @Test
    public void testIfApplyWithAB10IsTrue() throws Exception {
        measure = measureMaxStopsRule.apply(question);
        Assert.assertEquals("AB10", measure.getOrigin()+measure.getDestination()+measure.getValue());
    }

    @Test
    public void testIfGetMaxStopsWithAB10() throws Exception {
        measure = measureMaxStopsRule.apply(question);
        Assert.assertEquals(Integer.valueOf(5), measureMaxStopsRule.getMaxStops(tripService));
    }

}
