package br.com.vagas.trains.service;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author wagner on 24/08/17
 */
public class MeasureServiceTest {
    private final MeasureService measureService = new MeasureService();

    @Test
    public void testIfIsValidStringMeasureWithStringMeasureAB3IsTrue() throws Exception {
        Assert.assertTrue(measureService.isValidStringMeasure("AB3"));
    }

    @Test
    public void testIfIsValidStringMeasureWithStringMeasureABIsFalse() throws Exception {
        Assert.assertFalse(measureService.isValidStringMeasure("AB"));
    }

    @Test
    public void testIfIsValidStringMeasureWithStringMeasureABCIsFalseWithNumberException() throws Exception {
        try {
            measureService.isValidStringMeasure("ABC");
            Assert.fail();
        }catch (NumberFormatException nfe){
            Assert.assertEquals("java.lang.NumberFormatException: For input string: \"C\"", nfe.toString());
        }
    }


}
