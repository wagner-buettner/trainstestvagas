package br.com.vagas.trains.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author wagner on 23/08/17
 */
public class GraphServiceTest {
    private GraphService graphService;

    @Before
    public void setUp() throws Exception {
        graphService = new GraphService();
    }

    @Test
    public void testIfIsValidGraphWithWAGNER() throws Exception {
        Assert.assertTrue(graphService.isValidGraph("W-A-G-N-E-R"));
    }

    @Test
    public void testIfIsValidGraphWithWAIsFalse() throws Exception {
        Assert.assertFalse(graphService.isValidGraph("WA"));
    }

    @Test
    public void testIfIsValidGraphWithWAGWithoutHyphenIsFalse() throws Exception {
        Assert.assertFalse(graphService.isValidGraph("WAG"));
    }

    @Test
    public void testIfIsValidGraphWithWAGWithHyphenIsTrue() throws Exception {
        Assert.assertTrue(graphService.isValidGraph("W-A-G"));
    }

    @Test
    public void testIfGraphValidatorWithWAGWithHyphenIsTrue() throws Exception {
        try {
            graphService.graphValidator("W-A-G");
            Assert.assertTrue(true);
        } catch (Exception ex) {
            Assert.fail();
        }
    }

    @Test
    public void testIfGraphValidatorWithWAGWithoutHyphenIsFalse() throws Exception {
        try {
            graphService.graphValidator("WAG");
        } catch (Exception ex) {
            Assert.assertTrue(true);
        }
    }
}
