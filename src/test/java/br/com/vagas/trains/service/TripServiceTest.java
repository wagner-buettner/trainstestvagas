package br.com.vagas.trains.service;

import br.com.vagas.trains.domain.Route;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;

/**
 * @author wagner on 23/08/17
 */
public class TripServiceTest {

    private TripService tripService;

    @Before
    public void setUp() throws Exception {
        tripService = new TripService();
        RouteService routeService = new RouteService();

        routeService.addRoute(new Route("A", "B", 5));
        routeService.addRoute(new Route("B", "C", 4));
        routeService.addRoute(new Route("C", "D", 8));
        routeService.addRoute(new Route("D", "C", 8));
        routeService.addRoute(new Route("D", "E", 6));
        routeService.addRoute(new Route("A", "D", 5));
        routeService.addRoute(new Route("C", "E", 2));
        routeService.addRoute(new Route("E", "B", 3));
        routeService.addRoute(new Route("A", "E", 7));

        tripService.setRouteService(routeService);
    }

    @Test
    public void testIfGetRouteServiceWith9GetRoutesSize() throws Exception {
        Assert.assertEquals(9, tripService.getRouteService().getRoutes().size());
    }

    @Test
    public void testIfGetRouteServiceIsNot0GetRoutes() throws Exception {
        Assert.assertNotEquals(0, tripService.getRouteService().getRoutes().size());
    }

    @Test
    public void testIfLoadRouteFileExistsWith9Routes() throws Exception {
        TripService tripServiceLoad;
        tripServiceLoad = new TripService();
        tripServiceLoad.loadRouteFile("routes.txt");
        Assert.assertEquals(9, tripServiceLoad.getRouteService().getRoutes().size());
    }

    @Test
    public void testIfLoadRouteFileFailLoadFile() throws Exception {
        TripService tripServiceLoad;
        tripServiceLoad = new TripService();
        try {
            tripServiceLoad.loadRouteFile("routeFail.txt");
            Assert.fail();
        }catch (FileNotFoundException fnf){

        }
    }

    @Test
    public void testIfQuantityOfTripsWithMaxStopsIs5() throws Exception {
        Assert.assertEquals(Integer.valueOf(5), tripService.quantityOfMaxStops("A", "B", 10));
    }

    @Test
    public void testIfShortestDistanceLengthWithAB5Is5() throws Exception {
        Assert.assertEquals(Integer.valueOf(5), tripService.shortestDistance("A", "B"));
    }

}

