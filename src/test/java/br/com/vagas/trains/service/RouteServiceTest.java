package br.com.vagas.trains.service;

import br.com.vagas.trains.domain.Route;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.Map;

/**
 * @author wagner on 23/08/17
 */
public class RouteServiceTest {

    private RouteService routeService;

    @Before
    public void setUp() throws Exception {
        routeService = new RouteService();
        routeService.addRoute(new Route("A", "B", 5));
        routeService.addRoute(new Route("B", "C", 4));
        routeService.addRoute(new Route("C", "D", 8));
        routeService.addRoute(new Route("D", "C", 8));
        routeService.addRoute(new Route("D", "E", 6));
        routeService.addRoute(new Route("A", "D", 5));
        routeService.addRoute(new Route("C", "E", 2));
        routeService.addRoute(new Route("E", "B", 3));
        routeService.addRoute(new Route("A", "E", 7));
    }

    @Test
    public void testGetRoutesWith9() {
        Assert.assertEquals(9, routeService.getRoutes().size());
    }

    @Test
    public void testGetRoutesNot10() {
        Assert.assertFalse(routeService.getRoutes().size() == 10);
    }

    @Test
    public void testIfLoadRouteFileExistsWith9Routes() throws Exception {
        new RouteService().loadRouteFile("routes.txt");
    }

    @Test
    public void testIfLoadRouteFileFailLoadFile() throws Exception {
        RouteService routeServiceLoad;
        routeServiceLoad = new RouteService();
        try {
            routeServiceLoad.loadRouteFile("routeFail.txt");
            Assert.fail();
        }catch (FileNotFoundException fnf){

        }
    }

    @Test
    public void testIfAddRouteWithWA10() {
        Route route = new Route("W", "A", 10);
        routeService.addRoute(route);
        Assert.assertEquals(route, routeService.getRoutes().get("WA"));
    }

    @Test
    public void testIfRouteListWithOriginA() throws Exception {
        Iterator<Map.Entry<String, Route>> iterator = routeService.routeListWithOrigin("A").entrySet().iterator();
        Route route;

        //compare all routes
        while (iterator.hasNext()) {
            route = iterator.next().getValue();
            Assert.assertEquals("A", route.getOrigin());
        }

    }

    @Test
    public void testIfRouteExists() {
        Assert.assertTrue(routeService.exists("E", "B"));
    }

}
