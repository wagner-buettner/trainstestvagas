package br.com.vagas.trains.service;

import org.junit.Assert;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @author wagner on 24/08/17
 */
public class RuleServiceTest {

    @Test
    public void testIfLoadRouteFileFail() throws IOException {
        try {
            new RuleService().loadRouteFile("routesFail.txt");
        } catch (FileNotFoundException fnf) {
            Assert.assertEquals("java.io.FileNotFoundException: routesFail.txt (Arquivo ou diretório não encontrado)"
                              , fnf.toString());
        }
    }

    @Test
    public void testIfRunRulesWithRoutesFailAndQuestions() throws Exception {
        try {
            new RuleService().runRules("routesFail.txt", "questions.txt");
        } catch (FileNotFoundException fnf) {
            Assert.assertEquals("java.io.FileNotFoundException: routesFail.txt (Arquivo ou diretório não encontrado)"
                               , fnf.toString());
        }
    }

    @Test
    public void testIfRunRulesWithRoutesAndQuestionsFail() throws Exception {
        try {
            new RuleService().runRules("routes.txt", "questionsFail.txt");
        } catch (FileNotFoundException fnf) {
            Assert.assertEquals("java.io.FileNotFoundException: questionsFail.txt (Arquivo ou diretório não encontrado)"
                               , fnf.toString());
        }
    }

    @Test
    public void testIfLoadRouteFileWithSucess() throws IOException {
        try {
            new RuleService().loadRouteFile("routes.txt");
        } catch (FileNotFoundException f) {
            throw new FileNotFoundException();
        }
    }

    @Test
    public void testIfRunRulesWithRoutesAndQuestions() throws Exception {
        try {
            new RuleService().runRules("routes.txt", "questions.txt");
        } catch (Exception ex) {
            Assert.fail();
        }
    }

}
