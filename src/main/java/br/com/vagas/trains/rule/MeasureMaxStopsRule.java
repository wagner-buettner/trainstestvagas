package br.com.vagas.trains.rule;

import br.com.vagas.trains.domain.Measure;
import br.com.vagas.trains.service.MeasureService;
import br.com.vagas.trains.service.TripService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author wagner on 17/08/17
 */
public class MeasureMaxStopsRule {
    private static final Logger logger = LogManager.getLogger(MeasureMaxStopsRule.class);

    final private Measure measure;
    final private MeasureService measureService;

    public MeasureMaxStopsRule() {
        measure = new Measure();
        measureService = new MeasureService();
    }

    public Measure apply(String question) {
        if (question == null) {
            logger.warn("Null question");
            return null;
        }

        String ruleDescription = "The number of trips starting at ";
        String replacedQuestion;

        if (question.startsWith(ruleDescription) && question.contains("with a maximum of")) {

            replacedQuestion = question.replace(ruleDescription, "");
            replacedQuestion = replacedQuestion.replace(" and ending at ", "");
            replacedQuestion = replacedQuestion.replace(" with a maximum of ", "");
            replacedQuestion = replacedQuestion.replace(" stops", "");
            replacedQuestion = replacedQuestion.replace(" stop", "");

            if (!measureService.isValidStringMeasure(replacedQuestion)) {
                logger.error("Invalid Question! {}", question);
            }

            measure.setOrigin(replacedQuestion.substring(0, 1));
            measure.setDestination(replacedQuestion.substring(1, 2));
            measure.setValue(Integer.parseInt(replacedQuestion.substring(2)));

            logger.info("Applying rule for Graph: {}, Origin: {}, Destination: {}, Value: {}"
                        , replacedQuestion
                        , replacedQuestion.substring(0, 1)
                        , replacedQuestion.substring(1, 2)
                        , replacedQuestion.substring(2));
            return measure;
        }

        return null;
    }

    public Integer getMaxStops(TripService tripService) {
        return tripService.quantityOfMaxStops(measure.getOrigin()
                                                     , measure.getDestination()
                                                     , measure.getValue());
    }
}
