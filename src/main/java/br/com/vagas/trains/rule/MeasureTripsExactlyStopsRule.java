package br.com.vagas.trains.rule;

import br.com.vagas.trains.domain.Measure;
import br.com.vagas.trains.service.MeasureService;
import br.com.vagas.trains.service.TripService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author wagner on 20/08/17
 */
public class MeasureTripsExactlyStopsRule {
    private static final Logger logger = LogManager.getLogger(MeasureTripsExactlyStopsRule.class);

    final private Measure measure;
    final private MeasureService measureService;

    public MeasureTripsExactlyStopsRule() {
        measure = new Measure();
        measureService = new MeasureService();
    }

    public Measure apply(String question) {
        if (question == null) {
            return null;
        }

        String baseQuestion = "The number of trips starting at ";
        String replacedQuestion;

        if (question.startsWith(baseQuestion)) {

            replacedQuestion = question.replace(baseQuestion, "");
            replacedQuestion = replacedQuestion.replace(" and ending at ", "");
            replacedQuestion = replacedQuestion.replace(" with exactly ", "");
            replacedQuestion = replacedQuestion.replace(" stops", "");
            replacedQuestion = replacedQuestion.replace(" stop", "");

            if (!measureService.isValidStringMeasure(replacedQuestion)) {
                logger.error("Invalid Questions! {}", question);
            }

            measure.setOrigin(replacedQuestion.substring(0, 1));
            measure.setDestination(replacedQuestion.substring(1, 2));
            measure.setValue(Integer.parseInt(replacedQuestion.substring(2)));

            logger.info("Setting Exactly Stops Rule for Graph: {}, Origin: {}, Destination: {}, Value: {}"
                        , replacedQuestion
                        , replacedQuestion.substring(0, 1)
                        , replacedQuestion.substring(1, 2)
                        , replacedQuestion.substring(2));

            return measure;
        }

        return null;
    }

    public Integer getExactlyStops(TripService tripService){
        return tripService.quantityOfExactlyStops(measure.getOrigin()
                                                     , measure.getDestination()
                                                     , measure.getValue());
    }
}

