package br.com.vagas.trains.rule;

import br.com.vagas.trains.domain.Measure;
import br.com.vagas.trains.service.GraphService;
import br.com.vagas.trains.service.TripService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author wagner on 20/08/17
 */
public class MeasureDistanceRule {
    private static final Logger logger = LogManager.getLogger(MeasureDistanceRule.class);

    final private Measure measure;
    final private GraphService graphService;

    public MeasureDistanceRule() {
        measure = new Measure();
        graphService = new GraphService();
    }

    public Measure apply(String question) {

        if (question == null) {
            logger.warn("Null question");
            return null;
        }

        String baseQuestion = "The distance of the route ";
        String replacedQuestion;

        if (question.startsWith(baseQuestion)) {
            replacedQuestion = question.replace(baseQuestion, "");

            if (replacedQuestion.length() > 0 && graphService.isValidGraph(replacedQuestion)) {

                measure.setGraph(replacedQuestion);

                logger.info("Applying rule for Graph: {}", replacedQuestion);
                return measure;
            }

        }

        logger.error("Error Applying rule for Graph: {}", question);
        return null;
    }

    public Integer convertStringGraphToDistance(String graph, TripService tripService) {
        graphService.graphValidator(graph);

        String origin = graph.substring(0, 1);
        String destination = "";
        Integer distance = 0;

        logger.info("Converting Graph to Distance: {}", graph);
        //starting for with 1 because the first position = origin
        for (int i = 1; i < graph.length(); i++) {

            //only even positions
            if (i % 2 == 0 && destination.isEmpty()) {
                destination = graph.substring(i, i + 1);

                //if nonExist route
                if (!tripService.getRouteService().exists(origin, destination)) {
                    logger.warn("Nonexistent Route:" + origin + destination);
                }

                if (tripService.getRouteService().getRoutes().containsKey(origin + destination)) {
                    distance += tripService.getRouteService().getRoutes().get(origin + destination).getDistance();
                }

                //origin = nextDestination
                origin = destination;
                destination = "";
            }

        }

        return distance;
    }

}