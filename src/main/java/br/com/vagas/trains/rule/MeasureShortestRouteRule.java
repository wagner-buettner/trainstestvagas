package br.com.vagas.trains.rule;

import br.com.vagas.trains.domain.Measure;
import br.com.vagas.trains.service.MeasureService;
import br.com.vagas.trains.service.TripService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author wagner on 20/08/17
 */
public class MeasureShortestRouteRule {
    private static final Logger logger = LogManager.getLogger(MeasureTripsExactlyStopsRule.class);

    final private Measure measure;
    final private MeasureService measureService;

    public MeasureShortestRouteRule() {
        measure = new Measure();
        measureService = new MeasureService();
    }

    public Measure apply(String question) {

        if (question == null) {
            logger.warn("Null question");
            return null;
        }

        String baseQuestion = "The length of the shortest route from ";
        String replacedQuestion;

        if (question.startsWith(baseQuestion)) {
            replacedQuestion = question.replace(baseQuestion, "");
            replacedQuestion = replacedQuestion.replace(" to ", "");

            if (!measureService.isValidStringMeasureDistance(replacedQuestion)) {
                logger.error("Invalid question! {}", question);
            }

            measure.setOrigin(replacedQuestion.substring(0, 1));
            measure.setDestination(replacedQuestion.substring(1, 2));

            return measure;
        }

        return null;
    }

    public Integer getShortestRoute(TripService tripService){
        return tripService.shortestDistance(measure.getOrigin()
                                          , measure.getDestination());
    }
}

