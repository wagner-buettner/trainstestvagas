package br.com.vagas.trains.rule;

import br.com.vagas.trains.domain.Measure;
import br.com.vagas.trains.service.MeasureService;
import br.com.vagas.trains.service.TripService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author wagner on 20/08/17
 */
public class MeasureDifferentRoutesLessStopsRule {
    private static final Logger logger = LogManager.getLogger(MeasureDifferentRoutesLessStopsRule.class);

    final private Measure measure;
    final private MeasureService measureService;

    public MeasureDifferentRoutesLessStopsRule() {
        measure = new Measure();
        measureService = new MeasureService();
    }

    public Measure apply(String question) {

        if (question == null) {
            return null;
        }

        String baseQuestion = "The number of different routes from ";
        String replacedQuestion;

        if (question.startsWith(baseQuestion)) {

            replacedQuestion = question.replace(baseQuestion, "");
            replacedQuestion = replacedQuestion.replace(" to ", "");
            replacedQuestion = replacedQuestion.replace(" with a distance of less than ", "");

            if (!measureService.isValidStringMeasure(replacedQuestion)) {
                logger.error("Invalid question! {}", question);
            }

            measure.setOrigin(replacedQuestion.substring(0, 1));
            measure.setDestination(replacedQuestion.substring(1, 2));
            measure.setValue(Integer.parseInt(replacedQuestion.substring(2)));

            logger.info("Applying rule for Graph: {}, Origin: {}, Destination: {}, Value: {}"
                        , replacedQuestion
                        , replacedQuestion.substring(0, 1)
                        , replacedQuestion.substring(1, 2)
                        , replacedQuestion.substring(2));
            return measure;
        }

        return null;
    }

    public Integer getDifferentRoutesWithLessStops(TripService tripService) {
        return tripService.quantityOfRoutesWithLessStops(measure.getOrigin()
                                                       , measure.getDestination()
                                                       , measure.getValue());
    }
}
