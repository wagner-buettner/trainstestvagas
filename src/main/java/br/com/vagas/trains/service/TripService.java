package br.com.vagas.trains.service;

import br.com.vagas.trains.domain.Path;
import br.com.vagas.trains.domain.Route;
import br.com.vagas.trains.domain.Trip;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author wagner on 15/08/17
 */
public class TripService {
    private static final Logger logger = LoggerFactory.getLogger(RuleService.class);

    private RouteService routeService;
    final private List<Trip> trips;

    public TripService() {
        routeService = new RouteService();
        trips = new ArrayList<>();
    }

    private void addTrip(Path path){
        trips.add(new Trip(path.getPath(), path.getStops(), path.getDistance()));
    }

    public void setRouteService(RouteService routeService) {
        this.routeService = routeService;
    }

    public RouteService getRouteService() {
        return routeService;
    }

    public void loadRouteFile(String pathFile) throws IOException {
        routeService.loadRouteFile(pathFile);
    }

    private void tracePath(String origin, String destination) {
        logger.info("Tracing Path from Origin: {} to Destination: {}", origin, destination);
        Path path = new Path();
        trips.clear();
        tracePath(origin, destination, path);
    }

    private void tracePath(String origin, String destination, Path path) {
        Map<String, Route> routeOrigin = routeService.routeListWithOrigin(origin);

        Iterator<Map.Entry<String, Route>> iterator = routeOrigin.entrySet().iterator();
        Route route;

        while (iterator.hasNext()) {
            route = iterator.next().getValue();

            if (path.getPath().contains(route.getId())) {
                continue;
            }

            path.addRoute(route);

            if (route.getDestination().equals(destination)) {
                addTrip(path);
            } else {
                tracePath(route.getDestination(), destination, path);
            }

            path.removeRoute(route);
        }
    }

    public Integer shortestDistance(String origin, String destination) {
        tracePath(origin, destination);
        Integer shortTrip = 0;

        for (Trip trip : trips) {
            if (shortTrip.equals(0) || trip.getDistance() < shortTrip){
                shortTrip = trip.getDistance();
            }
        }

        return shortTrip;
    }

    public Integer quantityOfExactlyStops(String origin, String destination, Integer stops) {
        tracePath(origin, destination);
        Integer qtTrips = 0;

        for (Trip trip : trips) {
            if (trip.getStops().equals(stops)) {
                qtTrips++;
            }
        }

        return qtTrips;
    }

    public Integer quantityOfMaxStops(String origin, String destination, Integer maxStops) {
        tracePath(origin, destination);
        Integer qtTrips = 0;

        for (Trip trip : trips) {
            if (trip.getStops() <= maxStops){
                qtTrips++;
            }
        }

        return qtTrips;
    }

    public Integer quantityOfRoutesWithLessStops(String origin, String destination, Integer maxDistance) {
        tracePath(origin, destination);
        Integer qtTrips = 0;

        for (Trip trip : trips) {
            if (trip.getDistance() <= maxDistance){
                qtTrips++;
            }
        }

        return qtTrips;
    }

}
