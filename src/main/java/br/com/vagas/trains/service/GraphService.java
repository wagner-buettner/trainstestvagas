package br.com.vagas.trains.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author wagner on 17/08/17
 */
public class GraphService {
    private static final Logger logger = LogManager.getLogger(GraphService.class);

    public boolean isValidGraph(String graph) {
        if (graph == null || graph.length() < 3) {
            logger.warn("Invalid graph given: {}", graph);
            return false;
        }

        for (int i = 0; i < graph.length(); i++) {
            if (i % 2 == 0 && graph.substring(i, i + 1).equals("-")) {
                return false;
            }

            if (i % 2 != 0 && !graph.substring(i, i + 1).equals("-")){
                return false;
            }
        }

        return true;
    }

    public void graphValidator(String graph) throws UnsupportedOperationException {
        if (!isValidGraph(graph)){
            throw new UnsupportedOperationException("Invalid format: " + graph);
        }
    }

}
