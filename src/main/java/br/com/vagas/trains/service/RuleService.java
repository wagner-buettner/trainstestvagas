package br.com.vagas.trains.service;

import br.com.vagas.trains.domain.Measure;
import br.com.vagas.trains.rule.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author wagner on 17/08/17
 */
public class RuleService {
    private static final Logger logger = LoggerFactory.getLogger(RuleService.class);

    final private TripService tripService;
    private Measure measure;
    final private MeasureMaxStopsRule measureMaxStopsRule;
    final private MeasureDistanceRule measureDistanceRule;
    final private MeasureTripsExactlyStopsRule measureTripsExactlyStopsRule;
    final private MeasureShortestRouteRule measureShortestRouteRule;
    final private MeasureDifferentRoutesLessStopsRule measureDifferentRoutesLessStopsRule;

    public RuleService() {
        super();
        tripService = new TripService();
        measure = new Measure();
        measureMaxStopsRule = new MeasureMaxStopsRule();
        measureDistanceRule = new MeasureDistanceRule();
        measureTripsExactlyStopsRule = new MeasureTripsExactlyStopsRule();
        measureShortestRouteRule = new MeasureShortestRouteRule();
        measureDifferentRoutesLessStopsRule = new MeasureDifferentRoutesLessStopsRule();
    }

    public void loadRouteFile(String file) throws IOException {
        tripService.loadRouteFile(file);
    }

    public void runRules(String routeFile, String questionFile) throws IOException {
        loadRouteFile(routeFile);

        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(questionFile), "UTF-8"));
        logger.info("Opening questionFile: {}", questionFile);

        String line = br.readLine();

        while (line != null) {
            if (!line.isEmpty()) {
                applyRules(line);
            }
            line = br.readLine();
        }

        br.close();
        logger.info("Closing questionFile: {}", questionFile);
    }

    private void convertQuestionToType(String question) {
        final String maxStops = "The number of trips starting at ";
        final String exactlyStops = "The number of trips starting at ";
        final String distanceRoute = "The distance of the route ";
        final String shortestDistance = "The length of the shortest route from";
        final String qtDifferentRoutes = "The number of different routes from ";

        if (question.startsWith(maxStops) && question.contains("with a maximum of")){
            measure.setType("MAX-STOPS");
        }
        if (question.startsWith(distanceRoute)){
            measure.setType("DISTANCE");
        }
        if (question.startsWith(exactlyStops) && question.contains("exactly")){
            measure.setType("EXACTLY-STOPS");
        }
        if (question.startsWith(shortestDistance)){
            measure.setType("SHORTEST-ROUTE");
        }
        if (question.startsWith(qtDifferentRoutes) && question.contains("with a distance of less than")){
            measure.setType("DIFFERENT-ROUTES-WITH-LESS-STOPS");
        }
    }

    @SuppressWarnings("PMD")
    private void printResult(String question, Integer qtMeasure) {
        if (qtMeasure == null) {
            System.out.println("Invalid question! " + question);
            logger.warn("Invalid question! {}", question);
        } else if (qtMeasure == 0) {
            System.out.println(question + ": NO SUCH ROUTE");
            logger.warn("NO SUCH ROUTE! {}", question);
        } else {
            System.out.println(question + ": " + qtMeasure);
            logger.info(question + ": " + qtMeasure);
        }
    }

    private void applyRules(String question) {
        Integer ret;
        convertQuestionToType(question);

        if (measure.getType() == ("MAX-STOPS")) {
            measure = measureMaxStopsRule.apply(question);

            ret = measureMaxStopsRule.getMaxStops(tripService);
            printResult(question, ret);
        }

        if (measure.getType() == ("DISTANCE")) {
            measure = measureDistanceRule.apply(question);

            ret = measureDistanceRule.convertStringGraphToDistance(measure.getGraph(), tripService);
            printResult(question, ret);
        }

        if (measure.getType() == ("EXACTLY-STOPS")) {
            measure = measureTripsExactlyStopsRule.apply(question);

            ret = measureTripsExactlyStopsRule.getExactlyStops(tripService);
            printResult(question, ret);
        }

        if (measure.getType() == ("SHORTEST-ROUTE")) {
            measure = measureShortestRouteRule.apply(question);

            ret = measureShortestRouteRule.getShortestRoute(tripService);
            printResult(question, ret);
        }

        if (measure.getType() == ("DIFFERENT-ROUTES-WITH-LESS-STOPS")) {
            measure = measureDifferentRoutesLessStopsRule.apply(question);

            ret = measureDifferentRoutesLessStopsRule.getDifferentRoutesWithLessStops(tripService);
            printResult(question, ret);
        }
    }

}
