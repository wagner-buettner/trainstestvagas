package br.com.vagas.trains.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author wagner on 20/08/17
 */
public class MeasureService {
    private static final Logger logger = LogManager.getLogger(MeasureService.class);

    public boolean isValidStringMeasure(String Measure) throws NumberFormatException {

        if (Measure.length() < 3) {
            logger.warn("Invalid Measure: {}", Measure);
            return false;
        } else {
            try {
                Integer.parseInt(Measure.substring(2));
            } catch (NumberFormatException nfe) {
                logger.error("Invalid measure: {}", Measure);
                throw nfe;
            }
        }
        logger.info("Valid Measure: {}", Measure);
        return true;
    }

    public boolean isValidStringMeasureDistance(String Measure) throws UnsupportedOperationException{

        if (Measure.length() > 2){
            logger.warn("Invalid Distance to be Calculated: {}", Measure);
            return false;
        }
        logger.info("Valid Measure Distance: {}", Measure);
        return true;
    }
}
