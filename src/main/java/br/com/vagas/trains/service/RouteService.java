package br.com.vagas.trains.service;

import br.com.vagas.trains.domain.Route;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author wagner on 14/08/17
 */
public class RouteService {
    private static final Logger logger = LogManager.getLogger(RouteService.class);

    final private Map<String, Route> routes;

    public RouteService() {
        super();
        routes = new LinkedHashMap<>();
    }

    public Map<String, Route> getRoutes() {
        return routes;
    }

    public void loadRouteFile(String routeFile) throws IOException {
        logger.info("Opening routeFile: {}", routeFile);
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(routeFile), "UTF-8"));
        String line = br.readLine();

        while (line != null) {
            if (!line.isEmpty()) {
                this.addRoute(convertStringToRoute(line));
            }
            line = br.readLine();
        }

        br.close();
        logger.info("Closing routeFile: {} ", routeFile);
    }

    public void addRoute(Route route) throws UnsupportedOperationException {
        if (route.getId() == null) {
            throw new UnsupportedOperationException("Father Ozzy says: It's not possible a Road To Nowhere! Null route");
        }

        if (routes.containsKey(route.getId())) {
            throw new UnsupportedOperationException("Duplicated Route:" + route.getId());
        }

        routes.put(route.getOrigin() + route.getDestination(), route);
    }

    private boolean routeValidator(Route route) {

        return route.getOrigin() != null
                && route.getDestination() != null
                && !route.getOrigin().equals(route.getDestination());
    }

    private Route convertStringToRoute(String sRoute) throws UnsupportedOperationException {

        if (sRoute == null) {
            throw new UnsupportedOperationException("Route cannot be Null!");
        }

        try {
            Integer.parseInt(sRoute.substring(2));
        } catch (NumberFormatException nfe) {
            logger.error("Invalid route format: {} {} {}", sRoute,nfe.getMessage(), nfe);
        }

        Route route = new Route(sRoute.substring(0, 1)
                              , sRoute.substring(1, 2)
                              , Integer.parseInt(sRoute.substring(2)));

        if (!routeValidator(route)) {
            throw new UnsupportedOperationException("Paranoid Route: " + route + " is not valid");
        }

        return route;
    }

    public Map<String, Route> routeListWithOrigin(String origin) {
        Iterator<Map.Entry<String, Route>> iterator = routes.entrySet().iterator();

        Route route;
        Map<String, Route> routeOrigin = new LinkedHashMap<>();

        while (iterator.hasNext()) {
            route = iterator.next().getValue();

            if (route.getOrigin().equals(origin)) {
                routeOrigin.put(route.getOrigin() + route.getDestination(), route);
            }
        }
        return routeOrigin;
    }

    public boolean exists(String origin, String destination) {
        return routes.containsKey(origin + destination);
    }
}
