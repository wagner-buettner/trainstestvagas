package br.com.vagas.trains;

import br.com.vagas.trains.service.RuleService;

import java.io.IOException;
import java.util.Scanner;

//avoiding println pmd
@SuppressWarnings("PMD")
class CrazyTrain {

    public static void main(String[] args) throws IOException {
        try {
            Scanner scannerRoute = new Scanner(System.in, "UTF-8");
            System.out.print("To get in the CrazyTrain, tell me the route file: ");
            String routeFile = scannerRoute.next();

            Scanner scannerQuestion = new Scanner(System.in, "UTF-8");
            System.out.print("Now tell me the questions file: ");
            String questionFile = scannerQuestion.nextLine();

            //String routeFile = "routes.txt";
            //String questionFile = "questions.txt";

            RuleService ruleService = new RuleService();
            ruleService.runRules(routeFile, questionFile);
        } catch (IOException ex) {
            System.out.println(ex.toString());
            System.out.println("File Not Found. Road To NoWhere!");
        }

    }

}
