package br.com.vagas.trains.domain;

public class Route {
    private String id;
    final private String origin;
    final private String destination;
    final private Integer distance;

    public Route(String origin, String destination, Integer distance) {
        super();
        this.origin = origin;
        this.destination = destination;
        this.distance = distance;
        this.setId();
    }

    private void setId() {
        this.id = this.origin + this.destination;
    }

    public String getId() {
        return id;
    }

    public String getOrigin() {
        return origin;
    }

    public String getDestination() {
        return destination;
    }

    public Integer getDistance() {
        return distance;
    }
}
