package br.com.vagas.trains.domain;

public class Trip {
    private final String graph;
    private final Integer stops;
    private final Integer distance;

    public Trip(String graph, Integer stops, Integer distance) {
        super();
        this.graph = graph;
        this.stops = stops;
        this.distance = distance;
    }

    public String getGraph() {
        return graph;
    }

    public Integer getStops() {
        return stops;
    }

    public Integer getDistance() {
        return distance;
    }

}
