package br.com.vagas.trains.domain;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class Path {
    private final Map<String, Route> routes;
    private Integer distance;
    private Integer stops;

    public Path() {
        super();
        this.routes = new LinkedHashMap<>();
        this.distance = 0;
        this.stops = 0;
    }

    public void addRoute(Route route) {
        if (route.getId() == null) {
            throw new UnsupportedOperationException("Father Ozzy says: It's not possible a Road To Nowhere! Null route");
        }

        this.routes.put(route.getOrigin() + route.getDestination(), route);
        calculatePathDistanceAndStops();
    }

    public void calculatePathDistanceAndStops() {
        Iterator<Map.Entry<String, Route>> iterator = routes.entrySet().iterator();
        Route route;
        this.distance = 0;
        this.stops = 0;

        while (iterator.hasNext()) {
            route = iterator.next().getValue();
            this.distance += route.getDistance();
            this.stops += 1;
        }
    }

    public void removeRoute(Route route) {
        if (route.getId() == null) {
            throw new UnsupportedOperationException("Father Ozzy says: I don't know a Null route");
        }

        if (routes.containsKey(route.getId())) {
            routes.remove(route.getId());
        } else {
            throw new UnsupportedOperationException("Goodbye to Romance: Route not found: " + route.getId());
        }
    }

    public String getPath() {
        Iterator<Map.Entry<String, Route>> iterator = routes.entrySet().iterator();
        Route route;

        StringBuilder stringBuilder = new StringBuilder("");

        while (iterator.hasNext()) {
            route = iterator.next().getValue();
            stringBuilder.append(route.getId());
        }

        return stringBuilder.toString();
    }


    public Integer getDistance() {
        return distance;
    }

    public Integer getStops() {
        return stops;
    }

    public Map<String, Route> getRoutes() {
        return routes;
    }
}
